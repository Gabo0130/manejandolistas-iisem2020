/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.util.coleciones_seed.*;

/**
 *
 * @author madar
 */
public class TestCompararListas {
    
    public static void main(String[] args) {
        ListaS<Integer> listaS=new ListaS();
        ListaCD<Integer> listaC=new ListaCD();
        crearListaSimple(listaS);
        crearListaCD(listaC);
        
    }
    
    
    public static void crearListaSimple(ListaS<Integer> listaS)
    {
        long inicio = System.currentTimeMillis();
       
        for(int i=0;i<100000;i++)
            listaS.insertarFin(i);
        
        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin - inicio)/1000);
        System.out.println("Tiempo de lista Simple:"+tiempo +" segundos");
        
    }
    
    
    public static void crearListaCD(ListaCD<Integer> listaC)
    {
    long inicio = System.currentTimeMillis();
       
        for(int i=0;i<100000;i++)
            listaC.insertarFin(i);
        
        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin - inicio)/1000);
        System.out.println("Tiempo de lista CD:"+tiempo +" segundos");
    }
    
}
