/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.coleciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class ListaS<T> {
    
    private Nodo<T> cabeza=null;
    private int cardinalidad=0;

    public ListaS() {
    }

    
    /**
     *  1. Crear el nodo
        2. Insertar el info en el nodo
        3.  El nuevo nodo su siguiente es cabeza
        4. Cabeza es ahora quién ? Nuevo nodo
        5. aumentar carnidalidad
     * @param info  que se desea almacenar
     */
    
    public void insertarInicio(T info)
    {
        // Paso 1.
        Nodo<T> nuevo=new Nodo();
        //Paso 2. 
        nuevo.setInfo(info);
        //Paso 3.
        nuevo.setSig(this.cabeza);
        //Paso 4.
        this.cabeza=nuevo;
        //Paso 5.
        this.cardinalidad++;
    }
    
    // arraylist x= .... , x.add(marco), x.add(juan) --> cabeza: marco
    // arraylist x= .... , x.add(juan),x.add(marco),  --> cabeza: juan   --> "método add"
    
    public int getCardinalidad() { //getTamanio o size()
        return cardinalidad;
    }

    @Override
    public String toString() {
        
        String msg="";
        
        for(Nodo<T> posicion=this.cabeza;posicion!=null; posicion=posicion.getSig())
        {
            msg+=posicion.getInfo().toString()+"->";
        }
        
        /*
        
        NO -->
        
            while(this.cabeza!=null)
        {   
        
                msg+=this.cabeza.getinfo().tostrng()
        cabeza=cabeza.getsig();
        }
        */
            
        
        return msg+"null";
        
    }
    
    public boolean esta(T info) // containtTo(..)
    {
    
        for(Nodo<T> posicion=this.cabeza;posicion!=null; posicion=posicion.getSig())
        {
            if(posicion.getInfo().equals(info))
                return true;
        }
     return false;   
    }     
    
    
    public void insertarFin(T info)
    {
     //1. Situación ocurre cuando la lista está vacía:
        if(this.esVacia())
            this.insertarInicio(info);
        else
        {
            try {
                Nodo<T> ultimo=getPos(this.cardinalidad-1);
                Nodo<T> nuevo=new Nodo(info,null);
                ultimo.setSig(nuevo);
                this.cardinalidad++;
                
            } catch (Exception ex) {
             System.err.println(ex.getMessage());
            }
        }
    }
    
    /**
     * Decorador
     * Inserta un dato al final de la lista , si y solo si, el dato no está
     * @param info el objeto que deseo insertar
     */
    
    public void insertarFin_NO_repetido(T info)
    {
    
        try{
            this.insertarFin_NO_repetido_excp(info);
        
        }catch(Exception e)
        {
            System.err.println(e.getMessage());
        }
    
    }
    
    
    private void insertarFin_NO_repetido_excp(T info)
    {
    
        if(this.esta(info))
            throw new RuntimeException("El dato no se puede insertar por que ya está registrado");
        
        this.insertarFin(info);
    
    }
    
    
    public void insertarFin_NO_repetido_version2(T info)
    {
    
         // Sólo tengan un ciclo
    
    }
    
    
    /**
     *  Obtiene un objeto almacenado en la lista a partir de su posición
     * @param pos la posición dentro de la lista
     * @return 
     */
    public T get(int pos)
    {
    
        try {
            Nodo<T> x=this.getPos(pos);
            return x.getInfo();
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
        
    }
    
    /**
     *  Cambia un objeto de la posición especificada
     * @param pos indice del objeto a cambiar
     * @param objetoNuevo un objeto nuevo
     */
    
    public void set(int pos, T objetoNuevo)
    {
    
        try {
            
            Nodo<T> x=this.getPos(pos);
             x.setInfo(objetoNuevo);
             
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            
        }
        
    }
    
    private Nodo<T> getPos(int posFin) throws Exception
    {
    if(this.esVacia() || posFin<0 || posFin>this.cardinalidad)
        throw new Exception("No se encuentra la posición del nodo:"+posFin);
    
    Nodo<T> x=this.cabeza;
    while (posFin>0)
    {
        x=x.getSig();
        posFin--;
    }
    return x;
    }
    
    
    public boolean esVacia()
    {
        return this.cabeza==null;
    }
    
    
}
